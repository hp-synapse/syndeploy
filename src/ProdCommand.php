<?php

namespace hpsynapse\syndeploy;

use hpsynapse\syndeploy\Deploy;

class ProdCommand extends Deploy
{
    protected $_mode = 'prod';
}