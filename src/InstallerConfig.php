<?php

namespace hpsynapse\syndeploy;

class InstallerConfig {
    static $appsBaseGitUrl = 'https://gitlab.com/hp-synapse/apps-base.git';
    static $appsGeneratorGitUrl = 'https://gitlab.com/hp-synapse/apps-generator.git';
}