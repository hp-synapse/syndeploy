<?php

namespace hpsynapse\syndeploy;

use hpsynapse\syndeploy\Deploy;

class CreateCommand extends Deploy
{
    protected $_mode = 'create';
}