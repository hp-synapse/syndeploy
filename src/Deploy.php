<?php

namespace hpsynapse\syndeploy;

// use ZipArchive;
// use RuntimeException;
// use GuzzleHttp\Client;
use Symfony\Component\Process\Process;
// use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
// use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use hpsynapse\syndeploy\InstallerConfig;

class Deploy extends Command
{
    protected $_mode = 'dev';

    protected $availableSystemEnvKey = ['mode','laravel_key','web_state_persistant','web_serviceworker','web_pushnotif'];
    protected $protectedPackageLocalKey = [];

    /**
     * Configure the command options.
     *
     * @return vo id
     */
    protected function configure()
    {
        $this
            ->setName($this->_mode)
            ->setDescription('Deploy development environtment')
            ->addArgument('gitproject', InputArgument::REQUIRED, 'Url clone GIT repository Project OR Project Code when create')
            ->addArgument('targetpath', InputArgument::OPTIONAL, 'target project relative path')
            ->addOption('composerPath', null,InputOption::VALUE_OPTIONAL, 'absolut filename to composer');
    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Deploy apps on '.$this->_mode.' mode...</info>');

        if($input->getOption('composerPath')){
            $composer = $input->getOption('composerPath');
        }else{
            $composer = $this->findComposer();
        }        

        if($input->getArgument('targetpath')){
            $targetPath = getcwd().DIRECTORY_SEPARATOR.$input->getArgument('targetpath');
            $appsPath = getcwd().DIRECTORY_SEPARATOR.$input->getArgument('targetpath');
            // $this->verifyApplicationDoesntExist($appsPath);
        }else{
            $targetPath = '.';
            $appsPath = getcwd();
        }
        
        $commands = [];
        //set global credential helper
        $commands[] = 'git config --global credential.helper store';
        //clone apps-base (project structure)
        $commands[] = 'git clone '.InstallerConfig::$appsBaseGitUrl.' '.$targetPath; 
        
        //jika menyertakan targetpath maka pindah directory ke sana
        if($targetPath) $commands[] = 'cd '.$targetPath;
        if($this->_mode!='create'){
            //clone project git repository
            $commands[] = 'git clone '.$input->getArgument('gitproject').' ./app/MainApp'; 
        } 
        $this->executeCommands($commands,$output);
        $commands = [];
        //-----------------------------------------------------------------------
        
        $output->writeln('<comment>Saat proses clone Repository utama, data credential git Anda tersimpan secara global di komputer.</comment>');
        $output->writeln('<comment>Jika tidak ingin menyimpan data credential git setelah proses syndeploy, silahkan execusi command dibawah ini setelah proses deploy selesai</comment>');
        $output->writeln('<info>git config --global --unset credential.helper</info>');
        
        //copy env
        $this->xcopy($appsPath.'/.env.example', $appsPath.'/.env');
        //copy composer.json
        $this->xcopy($appsPath.'/composer.json.default', $appsPath.'/composer.json'); 
        //copy package.json
        $this->xcopy($appsPath.'/package.json.default', $appsPath.'/package.json'); 
        //copy systemEnv
        $this->xcopy($appsPath.'/app/MainApp/config/system.json', $appsPath.'/app/MainApp/config/systemEnv.json'); 
        //copy packageLocalEnv
        // $this->xcopy($appsPath.'/app/MainApp/config/packageLocal.json', $appsPath.'/app/MainApp/config/packageLocalEnv.json');
        
        //jika selain production mode maka ubah status menjadi dev mode
        if($this->_mode!='prod'){
            $tmpSystemEnv = json_decode(file_get_contents($appsPath.'/app/MainApp/config/systemEnv.json'), true);
            $tmpSystemEnv['mode'] = 'dev';
            file_put_contents($appsPath.'/app/MainApp/config/systemEnv.json', json_encode($tmpSystemEnv, JSON_PRETTY_PRINT));
        }

        //load data composer untuk dimodif sesuai kebutuhan
        $composerJson = json_decode(file_get_contents($appsPath.'/composer.json'), true);
        //load config session
        $systemConfig = json_decode(file_get_contents($appsPath.'/app/MainApp/config/system.json'),true); 

        /**
         * jika tidak mode create maka
         */
        if($this->_mode!='create'){   
            
            /**
             * replace APP_KEY
             */
            $output->writeln('<comment>Set APP_KEY...</comment>');  
            $env = file_get_contents($appsPath.'/.env');   
            $env = str_replace('APP_KEY=','APP_KEY='.$systemConfig['laravel_key'],$env);
            if($this->_mode=='prod')
                $env = str_replace('APP_ENV=local','APP_KEY=production',$env);
            file_put_contents($appsPath.'/.env', $env);

            /**
             * copy assets & dist ke public
             */
            $output->writeln('<comment>Copy dist & assets...</comment>');
            // $this->xcopy($appsPath.'/app/MainApp/resources/dist', $appsPath.'/public/dist'); 
            $this->xcopy($appsPath.'/app/MainApp/resources/assets', $appsPath.'/public/assets'); 
        }

        $commands[] = 'cd '.$appsPath;

        //install apps-generator hanya saat development atau create saja
        if($this->_mode != 'prod'){
            $composerJson['require']['hp-synapse/apps-generator'] = 'dev-master';
            $composerJson['repositories'][] = [
                "name" => "hp-synapse/apps-generator",
                "type" => "git",
                "url" => InstallerConfig::$appsGeneratorGitUrl
            ];
            // $cmd = $composer.' config repositories.hp-synapse/apps-generator git '.InstallerConfig::$appsGeneratorGitUrl;
            // $cmd2 = $composer.' require hp-synapse/apps-generator:dev-master';
            // $output->writeln('<comment>Add apps-generator dependency :</comment>');
            // echo $cmd."\n";
            //tambahkan apps-generator depndency
            // $commands[] = $cmd;
            // $commands[] = $cmd2;
            // $commands[] = $composer.' config repositories.hp-synapse/apps-generator \'{"type": "path","url": "../apps-generator", "options": {"symlink": true}}\'';
        }      

        //tambah dependency package yang ada jika bukan mode create
        if($this->_mode!='create'){
            $packageConfig = json_decode(file_get_contents($appsPath.'/app/MainApp/config/package.json'),true);
            $packageLocalConfig = json_decode(file_get_contents($appsPath.'/app/MainApp/config/packageLocal.json'),true);
            foreach ($packageConfig as $key => $package) {
                //jika package vendor
                if(isset($packageLocalConfig[$key]['is_package'])?isset($packageLocalConfig[$key]['is_package']):$package['is_package']){
                    
                    $composerJson['require']["hp-synapse/".$package['package_dir']] = 'dev-master';
                    $composerJson['repositories'][] = [
                        "name" => "hp-synapse/".$package['package_dir'],
                        "type" => "git",
                        "url" => $package['git']
                    ];

                    // $cmd = $composer.' config repositories.hp-synapse/'.$package['package_dir'].' git '.$package['git'];
                    // $cmd2 = $composer.' require hp-synapse/'.$package['package_dir'].':dev-master';
                    // $output->writeln('<comment>Add package dependency :</comment>');
                    // echo $cmd."\n";
                    // $commands[] = $cmd;
                    // $commands[] = $cmd2;
                }
            }
        }

        //tambah dependency package perproject
        if(isset($systemConfig['dependency']) && isset($systemConfig['dependency']['composer']) && is_array($systemConfig['dependency']['composer'])){
            foreach ($systemConfig['dependency']['composer'] as $key => $value) {                
                $composerJson['require'][$key] = $value;
            }
        }
        
        file_put_contents($appsPath.'/composer.json', json_encode($composerJson, JSON_PRETTY_PRINT));

        $output->writeln('<comment>Install composer dependency...</comment>');      
        //install semua dependensy composer
        if($this->_mode=='prod'){
            $commands[] = $composer.' install --optimize-autoloader --no-dev';
        }else{
            $commands[] = $composer.' install';
        }
        $commands[] = 'php artisan config:cache';  
        
        //jika mode create maka init new project
        if($this->_mode=='create'){
            $commands[] = 'php artisan syndeploy:create '.$input->getArgument('gitproject');
        }
        
        //auto install npm dependency hanya saat development saja
        if($this->_mode != 'prod'){
            $output->writeln('<comment>Install npm dependency...</comment>');
            $commands[] = 'npm install';
            if($this->_mode == 'dev')
                $commands[] = 'npm run dev';
        }else{
            //load/download production build ke folder /public/dist dari repo
        }
        // $commands[] = 'git config --global --unset credential.helper';
        $this->executeCommands($commands,$output);$commands = []; 

        $output->writeln('<comment>Application ready!</comment>');

        //clone project
        // $projectName = 'proj-'.md5(time() . uniqid());
        // $projectDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$projectName; 
        //copy dist & assets global di project ke
        // symlink('../'.$projectName, 'app/Modules');
    }

    protected function executeCommands($commands,$output) {
        $process = new Process(implode(' && ', $commands), null, null, null, 1000);

        if ('\\' !== DIRECTORY_SEPARATOR && file_exists('/dev/tty') && is_readable('/dev/tty')) {
            $process->setTty(true);
        }
        
        $process->run(function ($type, $line) use ($output) {
            $output->write($line);
        });

        // $process->start();
        // foreach ($process as $type => $data) {            
        //     $output->write($data);
        //     $lastOutput = $type;
        //     // if ($process::ERR === $type) {
        //     //     return false;
        //     // }
        // }
        // if ($process::ERR === $lastOutput) {
        //     return false;
        // }
        // return true;
        // $process->run(function ($type, $line) use ($output) {
        //     $output->write($line);
        // });
    }

    /**
     * Verify that the application does not already exist.
     *
     * @param  string  $directory
     * @return void
     */
    protected function verifyApplicationDoesntExist($directory)
    {
        if ((is_dir($directory) || is_file($directory)) && $directory != getcwd()) {
            die('Application already exists!');
        }
    }

    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer()
    {
        $composerPath = getcwd().'/composer.phar';

        if (file_exists($composerPath)) {
            return '"'.PHP_BINARY.'" '.$composerPath;
        }

        return 'composer';
    }

    /**
     * Copy a file, or recursively copy a folder and its contents
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.0.1
     * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @param       int      $permissions New folder creation permissions
     * @return      bool     Returns true on success, false on failure
     */
    protected function xcopy($source, $dest, $permissions = 0755)
    {
        //jika file/direktori tidak ada langsung tolak
        if(!file_exists($source))return true;

        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        
        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            $this->xcopy("$source/$entry", "$dest/$entry", $permissions);
        }

        // Clean up
        $dir->close();
        return true;
    }
}
